
# Changelog

This document follows the [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format.

Releases are available on [PyPI as `savior`](https://pypi.org/project/savior/).

## 0.2.3 - 2019-09-06
### Added
- added `Transaction.delete` method to permanently remove an entity

## 0.2.2 - 2019-08-06
### Fixed
- fixed bug where `created_at` dates were all from the first entity

## 0.2.1 - 2019-08-06
### Changed
- opening a database adds a schema version identifier integer to databases that don't have one
### Added
- added `Database.VERSION` for getting storage format version
- added `created_at` option to `query` to add an attribute to entities indicating when they were created

## 0.2.0 - 2019-07-15
### Removed
- removed `Database.open_tables` since tables are now required when opening a database
### Changed
- renamed `Transaction.create` to `Transaction.store` for consistency with `Transaction.fetch`
- changed `Database.open` behavior to require table names on initialization
- changed `Transaction` methods to raise error if a table name is used that was not used to open the database with

## 0.1.2 - 2019-07-15
### Added
- `Database.create_tables` to create tables in database
- `Database.open_tables` to open existing tables in database
- `Database.close` to close database manually

## 0.1.1 - 2019-07-15
### Added
- options for manually closing transactions for when it's not possible to use context managers

## 0.1.0 - 2019-07-15
### Added
- initial schema-less immutable interface over LMDB

